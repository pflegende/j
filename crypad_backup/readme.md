# Testbackup crypad

backup;
D2::P:\SGnachZohoNov22\backup\envs.cryptpad\ini

instruction on 

https://pad.envs.net/settings/#drive

Backup

Erstelle ein Backup deiner Daten im CryptDrive oder stelle die Daten wieder her. Es wird nicht den Inhalt der Dokumente beinhalten, sondern nur die Schlüssel für den Zugriff.
Lade alle Dokumente in deinem Drive herunter. Die Dokumente werden in einem für andere Anwendungen lesbaren Format heruntergeladen, sofern dies möglich ist. Sollte dies nicht möglich sein, werden die Dokumente in einem für CryptPad lesbaren Format heruntergeladen.

Importieren

Importiere die kürzlich besuchten Dokumente in dein CryptDrive
