# j

j - log & template
aka journal
ini == 6.8.2023, 01:57:59
last logout == 

- 27.1.2025, 07:27:25
- 5.1.2025, 02:19:57
- 24.12.2024, 02:35:35
- 11.11.2024, 21:07:09
- 21.10.2024, 22:30:34
- 15.9.2024, 02:47:36
- 26.8.2024, 23:30:13
- 16.7.2024, 12:36:21
- 25.6.2024, 10:01:09
- 21.5.2024, 04:09:22
- 22.4.2024, 22:12:49
- 20.3.2024, 21:47:59
- 18.2.2024, 03:35:42
- 17.1.2024, 22:43:51
- 25.12.2023, 21:07:23
- 27.11.2023, 21:07:23
- 20.11.2023, 21:07:02
- 2.11.2023, 13:45:16 

## next: onboarding envs.net 

### 2do
- https://pad.envs.net/register/  - check ob markdown, may be: https://pad.envs.net/code/  -> man https://cryptpad.org/
- backup pad! API like etherpad.foo/p/foo/export.txt ?
  - man ∑ cryptpad https://pad.envs.net/features.html
  - man cryptpad https://docs.cryptpad.org/en/
- connect Tilde & envs to GUUG & LS8
- ssh -> https://envs.net/signup/ -> the repo around this: https://git.envs.net/envs
- check rele https://envs.net/
- announce on erfurt.chat
- [ADV operate!](https://docs.cryptpad.org/de/FAQ.html#can-you-provide-a-data-processing-agreement-dpa) 

¯\\_(ツ)_/¯ 


## result of 2do

- [**issue**](https://git.envs.net/pflegende/j/issues/1) ssh -> https://envs.net/signup/ -> the repo around this: https://git.envs.net/envs
- **ok** workaround cyptpad backup; P:\SGnachZohoNov22\backup\envs.cryptpad\ini 6.8.2023, 13:05 
- **ok** register https://pad.envs.net/register/ defa defa
- **ok** [ADV seen](https://docs.cryptpad.org/de/FAQ.html#can-you-provide-a-data-processing-agreement-dpa)
- **ok** https://mobilizon.envs.net/events/me -> https://mobilizon.envs.net/events/2e06ef7f-5678-41fa-98ae-0c64f3fb0761
- **note** 
  [Liste der Erweiterungen, die bekanntermaßen Probleme mit CryptPad verursachen :](https://docs.cryptpad.org/de/FAQ.html#how-to-import-export-my-documents-to-from-another-platform)
```
Librejs

Adblock

Night Eye

uMatrix

JShelter

AdGuard

SuperAgent

Tampermonkey

Arkenfox (bestimmte Konfigurationen)
```


---
🚧🚧🚧 :EOF: 🚧🚧🚧
